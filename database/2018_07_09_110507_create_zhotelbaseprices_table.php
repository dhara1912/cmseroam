<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelbasepricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotelbaseprices')){
        Schema::create('zhotelbaseprices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_price_id')->nullable();
            $table->decimal('base_price',16,2);
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotelbaseprices');
    }
}
