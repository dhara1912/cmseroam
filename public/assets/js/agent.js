var Agent = function() {
	this.__construct = function() {
		this.addForm();
		this.loadList();
		this.search();
		this.switchDomain();
	};

	this.addForm = function() {
		$(document).on('submit','.add-form',function(evt) {
			evt.preventDefault();
			var pathname = window.location.pathname;
			var url = $(location).attr('href');
			var parts = url.split("/");
			var last_part = parts[parts.length-1];
			
			showLoader();
			var url = $(this).attr('action');
			var postData = $(this).serialize();
			var form = $(this)[0];
			var e = $(this);
			$.post(url,postData,function(out) {
				$(".error").remove();
				if(out.result === 0) {
					for(var i in out.errors) {
						hideLoader();
						$("#"+i).parent('.form-group').after('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">'+out.errors[i]+'</label>');
					}
				}
				if(out.result === 1) {
					$("#error_msg").addClass('alert-success').text(out.msg);
					$("html, body").animate({ scrollTop: 0 }, "slow");				
					if(out.url) {
						if(pathname=='/users/edit-customer/'+last_part){
							hideLoader();
							var new_site_url = $("#new_site_url").val()+"users/user-management";
							window.open(new_site_url);
						}else{
							agent.changeLocation();
							window.open(out.url,'_blank');
						}
						hideLoader();
					} else {
						hideLoader();
					}
				}
			});
		});
	};

	this.changeLocation = function(){
	 	setTimeout(function(){
	  		window.location = $('#new_site_url').val()+"agent/list-customer";
	 	},2000);
	}

	this.loadList = function() {
		$(document).ready(function() {
			if($('#list-wrapper').length){
				showLoader();
			}
			var url = $('#list-wrapper').data('url');
			$.get(url,'',function(out) {
				$("#list-wrapper").html(out.content);
				hideLoader();
			});
		});
	};	

	this.search = function() {
		$(document).on('click','#search_btn',function(evt) {
			evt.preventDefault();
			showLoader();
			var url = $(this).data('url');
			var search_by = $("#search_by").val();
			var search_text = $("#search_text").val();

			$.get(url,{search_by:search_by,search_text:search_text},function(out) {
				$("#list-wrapper").html(out.content);
				hideLoader();
			});
		});
	};

	this.switchDomain = function() {
		$(document).on('click','.switch-domain-btn',function(evt) {
			evt.preventDefault();
			showLoader();
			var url = $(this).attr('href');
			var domain_id = $(this).data('domain-id');
			$.post(url,{domain_id:domain_id},function(out) {
				if(out.success) {
					window.location.href = '/';
					hideLoader();
				}
			});
		});
	};

	this.__construct();
}
var agent = new Agent();