<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportMarkupSupplierCommission extends Model
{
    protected $table = 'ztransportmarkupsuppliercommissions';
    protected $primaryKey = 'id';
    protected $fillable = ['transport_markup_id','percentage'];
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
