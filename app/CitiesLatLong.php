<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CitiesLatLong extends Model
{
    protected $fillable = [
        'city_id','lat','lng'
    ];
    protected $table = 'zcitieslatlang';
    protected $primaryKey = 'id';
    public $timestamps = false; 
    
}
