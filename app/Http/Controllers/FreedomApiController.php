<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class FreedomApiController extends Controller 
{

	public function synchronize(Request $request) {
		$allData = $request->json()->all();
		try {
			foreach ($$allData as $traveller) {
				$email = $traveller['email'];
				$user = User::whereEmail($email)->first();
				if($user instanceof User) {
					$customer = Customer::whereEmail($email)->first();
					$customer->updateDetails($traveller);
				}
			}
			return respose()->setStatusCode(202);
		} catch (\Exception $ex) {
			return response()->setStatusCode(500);
		}
	}
}