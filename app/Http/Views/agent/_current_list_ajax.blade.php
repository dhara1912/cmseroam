<table class="table">
    <thead>
        <tr>
            <th onclick="getCurrentBookingSort(this,'username');">Client Name
                    <i class="{{ ($currentOrderBy == 'asc' && $currentOrderField == 'username')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>Number of PAX</th>
            <th onclick="getCurrentBookingSort(this,'created_at');">Saved Date
                 <i class="{{ ($currentOrderBy == 'asc' && $currentOrderField == 'created_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCurrentBookingSort(this,'travel_date');">Departure Date
                <i class="{{ ($currentOrderBy == 'asc' && $currentOrderField == 'travel_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>Itinerary Type</th>
            <th onclick="getCurrentBookingSort(this,'total_amount');">Total Price
                <i class="{{ ($currentOrderBy == 'asc' && $currentOrderField == 'total_amount')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>Cost Price</th>
            <th>Commission</th>
        </tr>
    </thead>
    <tbody class="user_list_ajax">
    @if(count($currentBookings) > 0)
        @include('WebView::agent._more_current_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>