@extends('layout/mainlayout')
@section('content')
<div class="content-container">
    <h1 class="page-title">Customers List</h1>
    <div class="box-wrapper">
    	<div class="row">
    	    <div class="col-sm-4">
    	        <div class="form-group m-t-0">
    	        	<label class="label-control">Search By</label>
    	            <select class="form-control" id="search_by" name="search_by">
    	                <option value="id">Account ID </option>
    	                <option value="username">Email Address </option>
    	                <option value="name">Name </option>
    	            </select>
    	        </div>
    	    </div>
    	    <div class="col-sm-4">
    	        <div class="form-group m-t-0">
    	    	<label class="label-control">Search Text</label>
    	            <input class="form-control" name="search_text" id="search_text" type="text" placeholder="Search..">
    	        </div>
    	    </div>
    	    <div class="col-sm-4">
    	        <input type="button" name="search" id="search_btn" data-url="{{ route('agent.list') }}" class="btn btn-primary btn-block" value="Search">
    	    </div>
    	</div>
    </div>
    <div class="box-wrapper">
    	<div id="list-wrapper" data-url="{{ route('agent.list') }}"></div>
    </div>
</div>
@endsection

@push('scripts')

<script type="text/javascript" src="{{ asset('assets/js/agent.js') }}"></script>
@endpush