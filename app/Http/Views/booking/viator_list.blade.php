@extends( 'layout/mainlayout' )
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }
</style>
@stop
@section('content')


@if($oItinerary)
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.viator_booking_info') }}</h1> 
    <div class="box-wrapper">  
        <div class="table-responsive m-t-20 table_record">
            <table class="table">
                <thead>
                    <tr>
                        <th onclick="getTourSort(this,'invoice_no');">{{ trans('messages.booking_id') }} 
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'invoice_no')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
                        </th>
                        <th onclick="getTourSort(this,'i.created_at');">{{ trans('messages.date') }} 
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.created_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
                        </th>                       
                        <th onclick="getTourSort(this,'first_name');">{{ trans('messages.customer') }} 
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'first_name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
                        </th>
                        <th onclick="getTourSort(this,'status');">{{ trans('messages.status') }} 
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'status')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
                        </th>
                        <th onclick="getTourSort(this,'total_amount');">{{ trans('messages.price') }} 
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'total_amount')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
                        </th>
                        <?php /*<th onclick="getTourSort(this,'voucher');">{{ trans('messages.voucher') }} 
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'voucher')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
                        </th>*/?>
                        <th class="text-center">{{ trans('messages.thead_action') }}</th>
                    </tr>
                </thead>
                <tbody class="tour_list_ajax">
                    @foreach ($oItinerary as $aItinerary)  
                    <?php $name ='Guest'; ?>
                    @foreach ($aItinerary['Passenger'] as $key => $value) 
                        <?php $name = ($value->is_lead == 'Yes') ? $value->first_name . ' ' . $value->last_name : 'Guest'; ?>
                    @endforeach
                    <tr>
                        
                        <td> <a href="{{ route('booking.show-itenary',['nItenaryId'=>$aItinerary->order_id]) }}">{{$aItinerary->booking_id}}</a></td>
                        <td>{{ date( 'd/m/y', strtotime( $aItinerary->created_at ) ) }}</td>
                     
                        <td>{{ $name }}</td>
                        <td>
                            <span data-status-id="{{ $aItinerary->order_id }}" >
                                <a href="javascript://" title="Update Status" class="update-status-btn"></a> 
                                <span class="status-name">{{ $aItinerary->provider_booking_status }}</span>
                            </span>
                            <div class="update-status-box"></div>
                        </td>
                        <td>{{ $aItinerary->currency }}$ {{ $aItinerary->price }}</td>
                        <?php /*<td>
                            <span data-voucher-id="{{ $aItinerary->order_id }}" >
                                <a href="javascript://" title="Update Voucher" class="update-voucher-btn"><i class="fa fa-pencil-square-o"></i></a> 
                                <span class="voucher-name">{{ $aItinerary->voucher }}</span>
                            </span>
                            <div class="update-voucher-box"></div>
                        </td> */ ?>
                        <td class="text-center">
                            <a href="{{ route('booking.show-viator-booking',['nBookingId'=>$aItinerary->booking_id])}}" class="button success tiny btn-primary btn-sm">View</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        
        </div>
    </div>
</div>


@else
<div class="row full-width-row">
    <div class="small-12 column">
        {{ trans('messages.no_record_found') }}
    </div>
</div>

@endif
@stop
@section('custom-js')
<script>
    function getTourSort(element,sOrderField)
    {
        if($(element).find( "i" ).hasClass('fa-caret-down'))
        {
            $(element).find( "i" ).removeClass('fa-caret-down');
            $(element).find( "i" ).addClass('fa-caret-up');
            $("input[name='order_field']").val(sOrderField);
            $("input[name='order_by']").val('desc');
        }
        else
        {
            $(element).find( "i" ).removeClass('fa-caret-up');
            $(element).find( "i" ).addClass('fa-caret-down');
            $("input[name='order_field']").val(sOrderField);
            $("input[name='order_by']").val('asc');
        }
        getMoreListing(siteUrl('booking/itenary-list?page=1'),event,'table_record');
    }
</script>
@stop
