@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }
</style>
@stop
@section('content')

<div class="content-container" style="overflow:hidden;">
    <h1 class="page-title">{{ trans('messages.vouchers_details') }}</h1> 

    @include('WebView::booking.review_booking_menu')
	@foreach($cities_arr as $key=>$val)
    <div class="box-wrapper">
		<p class="h4">Vouchers / Tickets: {{ucfirst($val)}}</p>
        <hr>
        <?php $i = 0; ?>
        @foreach($vouchers_details[$val] as $voucherkey=>$voucher_val)
		 <div class="box-wrapper">
        <div class="row">
            <p class="h4">{{ucfirst($voucherkey)}}</p>
            <hr>
            <form enctype="multipart/form-data" class="add-form" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control disabled" for="voucher_file<?php echo $voucher_val['leg_detail_id']; ?>">Upload</label>
                            <input type="file" class="form-control" id="voucher_file<?php echo $voucher_val['leg_detail_id']; ?>" name="voucher_file">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group" style="padding: 10px;">
                            <label class="radio-checkbox label_check m-t-10" for="checkbox-1<?php echo $voucher_val['leg_detail_id']; ?>" data-id="<?php echo $voucher_val['leg_detail_id']; ?>"><input type="checkbox" id="checkbox-1<?php echo $voucher_val['leg_detail_id']; ?>" name="voucher_status" value="1" data-id="<?php echo $voucher_val['leg_detail_id']; ?>">Not Requires</label>
                        </div>
                    </div>
                    </br>
                    <div class="col-sm-offset-2 col-sm-8">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <div class="row">
                                    <input type="hidden" id="checkbox_status<?php echo $voucher_val['leg_detail_id']; ?>" value="">
                                    <a style="margin-top: 10px;" type="sumbit" class="btn btn-primary btn-block update_voucher" data-url="{{ route('booking.update-voucher-pdf',['nItenaryId'=>$voucher_val['leg_detail_id']])}}" data-id='<?php echo $voucher_val['leg_detail_id']; ?>' >update</a>
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
            </form>
			</br>
            <?php if (trim($vouchers_details[$val][$voucherkey]['voucher_url']) != '') { ?>
                <div class="col-sm-12">
                    <p class="h4"> Voucher is available at  <a href="{{$voucher_val['voucher_url']}}">Click Here</a></p>
                </div>
            <?php } $i + 1; ?>
        </div>
		</div>
        @endforeach
    </div>  
    @endforeach
    <div class="col-sm-offset-2 col-sm-8">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="row">
                    <a href="{{ route('booking.booking-itenary-detail',['nItenaryId'=>$nBookId])}}" class="btn btn-primary btn-block">Previous</a>
                </div>
            </div>	
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".radio-checkbox").change(function () {
                var ids = $(this).data('id');
                if ($('#checkbox-1' + ids).is(":checked")) {
                    $("#checkbox_status" + ids).val('1');
                } else {
                    $("#checkbox_status" + ids).val('0');
                }
            });
        });

        $('.update_voucher').on('click', function () {
            var url = $(this).data('url');
            var id = $(this).data('id');
            var not_required = $('#checkbox_status' + id).val();
            var file_data = $("#voucher_file" + id)[0].files[0];
            var form_data = new FormData();
            form_data.append('not_required', not_required);
            form_data.append('file_data', file_data);
            $.ajax({
                url: url, // point to server-side PHP script 
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (message) {
                    //alert(message);              // display response from the PHP script, if any
                }
            });
            $('#voucher_file').val('');                     /* Clear the file container */
        });
    </script>
    @stop


