@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }
    @import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css");
    .panel-default 
    {
        border-color: transparent;
    }
    .panel-default > .panel-heading
    {
        background-color: transparent;
        border-color: transparent;
        padding: 0px 0;
    }
    .panel-group .title
    {
        cursor: pointer;
    }
    .panel-group .title span
    {
        font-size: 16px;
        font-family: 'Open Sans', sans-serif;
        color: #464646;
        font-weight: bold;
        text-transform: uppercase;
    }
    .panel-heading .title:before {

        font-family: FontAwesome;
        content:"\f106";
        font-size: 25px;
        padding-right: 10px;
        line-height: 25px;
        float: right;
    }
    .panel-heading .title.collapsed:before {
        font-size: 25px;
        padding-right: 10px;
        line-height: 25px;
        float: right;
        content:"\f107";
    }
    .panel-body{
        padding: 5px !important;
    }
</style>
@stop
@section('content')

<div class="content-container" style="overflow:hidden;">
    <h1 class="page-title">{{ trans('messages.product_details') }}</h1> 

    @include('WebView::booking.review_booking_menu')
	<?php $pos = 1 ?>
    @foreach($cities_arr as $key=>$val)
    <div class="box-wrapper">
        <p class="h4">Products Details: {{ucfirst($val)}}</p>
        <hr>
		@foreach($product_details_all[$val] as $productkey=>$productval)
        <?php
        if ($productval['supplier_id'] != '') {
            $supplier = $productval['supplier_id'];
            $product = "Dynamic";
        } elseif ($productval['provider'] != '') {
            $supplier = $productval['provider'];
            $product = "Dynamic";
        } else {
            $supplier = 'Eroam';
            $product = "Static";
        }

        if ($pos == 1) {
            $accordion_status = "";
            $accordion_icon_status = "in";
        } else {
            $accordion_status = "collapsed";
            $accordion_icon_status = "";
        }
        ?>
        <div class="box-wrapper">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne{{$productval['leg_detail_id']}}">  
                        <div class="title {{$accordion_status}}  tab{{$pos}}" data-role="title" data-toggle="collapse" href="#collapseOne{{$productval['leg_detail_id']}}" aria-expanded="false" aria-controls="collapseOne">
                            <p class="h4"><strong>{{ucfirst($productkey)}}</strong><p>
                        </div>
                    </div>
                    <div id="collapseOne{{$productval['leg_detail_id']}}" class="panel-collapse collapse {{$accordion_icon_status}}" role="tabpanel" aria-labelledby="headingOne{{$productval['leg_detail_id']}}">
                        <div class="panel-body">
                            <div class="box-body">
                                @if($productkey == 'hotel')

                                <div class="panel-body">
                                    <table class="table table-responsive">
                                        <tbody>
                                            <tr>
                                                <td>Product Name : {{$productval['leg_name']}}
                                                <td>Product Type / Description : {{!empty($productval['hotel_room_type_name'])?$productval['hotel_room_type_name']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Accommodation Address Details : N/A</td>
                                                <td>Number of Nights  : {{!empty($productval['nights']) ? $productval['nights']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Check In Date / Time :{{!empty($productval['city_from_date']) ? $productval['city_from_date']:'N/A'}}</td>
                                                <td>Check Out Date / Time : {{!empty($productval['city_to_date']) ? $productval['city_to_date']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Supplier  : {{$supplier}}</td>
                                                <td>Dynamic / Static Product : {{$product}}</td>
                                            </tr>
                                            <tr>
                                                <td>Retail Price : {{!empty($productval['reseller_cost']) ? $productval['reseller_cost']:'N/A'}}</td>
                                                <td>Sell Price :{{$productval['sellprice']}}</td>
                                            </tr>
                                            <tr>
                                                <td>Commission  : {{$productval['licensee_benefit']}}</td>
                                                <td>Supplier Booking ID :{{!empty($productval['booking_id']) ? $productval['booking_id']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                 <td>Booking Status : Active</td>
                                                <td>Additional Notes :{{!empty($productval['notes']) ? $productval['notes']:'N/A'}}</td>
                                            </tr>
                                        </tbody></table>
                                </div>
                                @elseif($productkey == 'activities')
                                <div class="panel-body">
                                    <table class="table table-responsive">
                                        <tbody>
                                            <tr>
                                                <td>Product Name : {{$productval['leg_name']}}
                                                <td>Product Type / Description : {{!empty($productval['hotel_room_type_name'])?$productval['hotel_room_type_name']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Number Purchased: {{$total_travller}}</td>
                                                <td>Date of Activity  : {{!empty($productval['city_from_date']) ? $productval['city_from_date']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Time of Activity: {{!empty($productval['activity_start_time']) ? $productval['activity_start_time']:'N/A'}}</td>
                                                <td>Duration of Activity : {{!empty($productval['duration']) ? $productval['duration']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Supplier  : {{$supplier}}</td>
                                                <td>Dynamic / Static Product : {{$product}}</td>
                                            </tr>
                                            <tr>
                                                <td>Retail Price : {{!empty($productval['hotel_expedia_total']) ? $productval['hotel_expedia_total']:'N/A'}}</td>
                                                <td>Sell Price :{{$productval['sellprice']}}</td>
                                            </tr>
                                            <tr>
                                                <td>Commission  : {{$productval['licensee_benefit']}}</td>
                                                <td>Supplier Booking ID :{{!empty($productval['booking_id']) ? $productval['booking_id']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Booking Status : Active</td>
                                                <td>Additional Notes :{{!empty($productval['notes']) ? $productval['notes']:'N/A'}}</td>
                                            </tr>
                                        </tbody></table>
                                </div>
                                @elseif($productkey == 'transport')
                                <div class="panel-body">
                                    <table class="table table-responsive">
                                        <tbody>
                                            <tr>
                                                <td>Product Name : {{$productval['leg_name']}}
                                                <td>Product Type / Description : {{!empty($productval['hotel_room_type_name'])?$productval['hotel_room_type_name']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Number Purchased: {{$total_travller}}</td>
                                                <td>Departure Details  : {{!empty($productval['departure_text']) ? $productval['departure_text']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Arrival Details : {{!empty($productval['arrival_text']) ? $productval['arrival_text']:'N/A'}}</td>
                                                <td>Stops (List of Locations - If Applicable) : {{!empty($productval['hotel_room_type_name']) ? $productval['hotel_room_type_name']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Duration : {{!empty($productval['duration']) ? $productval['duration']:'N/A'}}</td>
                                                <td>Class : N/A</td>
                                            </tr>
                                            <tr>
                                                <td>Baggage Allowance: N/A</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Supplier  : {{$supplier}}</td>
                                                <td>Dynamic / Static Product : {{$product}}</td>
                                            </tr>
                                            <tr>
                                                <td>Retail Price : {{!empty($productval['hotel_expedia_total']) ? $productval['hotel_expedia_total']:'N/A'}}</td>
                                                <td>Sell Price :{{$productval['sellprice']}}</td>
                                            </tr>
                                            <tr>
                                                <td>Commission  : {{$productval['licensee_benefit']}}</td>
                                                <td>Supplier Booking ID :{{!empty($productval['booking_id']) ? $productval['booking_id']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Booking Status : Active</td>
                                                <td>Additional Notes :{{!empty($productval['notes']) ? $productval['notes']:'N/A'}}</td>
                                            </tr>
                                        </tbody></table>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
    </div> 
    <?php $pos++ ?>	
    @endforeach
	
	<div class="m-t-20 row">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="row">
				<div class="col-sm-6">
					<a href="{{ route('booking.booking-location-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Previous</a>
				</div>
				<div class="col-sm-6">
				   <a href="{{ route('booking.booking-supplier-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Next</a>
				</div>
			</div>
		</div>
    </div>
    @stop