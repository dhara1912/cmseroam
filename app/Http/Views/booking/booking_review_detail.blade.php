@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }

    .address{
        margin-left: 45px;
        font-family: sans-serif;
        font-size: 14px;
    }
</style>
@stop
@section('content')

<div class="content-container" >
    <h1 class="page-title">PAX Details</h1> 
	@include('WebView::booking.review_booking_menu')
	<div class="box-wrapper">
        <p class="h4">{{ trans('messages.lead_pax_detail') }}</p>
        <hr>
        <h5><strong></strong></h5>
        </br>
        <div class="panel panel-default">
            <div class="panel-heading">PAX Details</div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tbody><tr>
                            <td>Account ID : {{$lead['itenary_order_id']}} </td>
                            <td>Title : {{$lead['title']}}</td>
                        </tr>

                        <tr>
                            <td>Given Name : {{$lead['first_name']}}</td>
                            <td>Family Name : {{$lead['last_name']}}</td>
                        </tr>

                        <tr>
                            <td>Other Names : N/A</td>
                            <td>Gender : {{$lead['gender']}}</td>

                        </tr>
                        <tr>
                            <td>Year of Birth : {{$lead['dob']}}</td>
                            <td>Nationality : {{!empty($lead['country'])? getCityName($lead['country']):'N/A'}}
							</td>
                        </tr>

                    </tbody></table>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Contact Details</div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tbody><tr>
                            <td>Phone : {{$lead['itenary_order_id']}} </td>
                            <td>Email Address : {{$lead['title']}}</td>
                        </tr>

                    </tbody></table>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Address</div>
                            <div class="panel-body">
                                <table class="table table-responsive">
                                    <tbody>
                                        <tr>
                                            <td>
												Address (Line One): {{!empty($lead['address_one'])? $lead['address_one']:'N/A'}}
											</td>
                                            <td>Address (Line Two): {{!empty($lead['address_two'])? $lead['address_two']:'N/A'}}</td>
                                        </tr>
                                        <tr>
                                            <td>City: {{!empty($lead['suburb'])? $lead['suburb']:'N/A'}}</td>
                                            <td>State: {{!empty($lead['state'])? $lead['state']:'N/A'}}</td>
                                        </tr>
                                        <tr>
                                            <td>Country: {{!empty($lead['country'])? getCityName($lead['country']):'N/A'}}  </td>
                                            <td>ZIP / Postal Code: {{!empty($lead['zip'])? $lead['zip']:'N/A'}}</td>
                                        </tr>

                                    </tbody></table>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table table-responsive">
                    <tbody><tr>
                            <td>Contact Method: Eroam </td>
                            <td></td>
                        </tr>

                    </tbody></table>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('messages.additional_information') }}</div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tbody>
                        <tr>
                            <td>Information : N/A </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>  
    </br>
    <div class="box-wrapper">
        <p class="h4">{{ trans('messages.additional_pax_detail') }}</p>
        <hr>
        <?php if (!empty($additional_arr)) { ?>
            <?php foreach ($additional_arr as $additional) { ?>
                <div class="panel panel-default">

                    <div class="panel-heading">PAX Details</div>
                    <div class="panel-body">
                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>Account ID : {{$additional['itenary_order_id']}} </td>
                                    <td>Title : {{!empty($additional['title'])? $additional['title']:'N/A'}}</td>
                                </tr>

                                <tr>
                                    <td>Given Name : {{!empty($additional['first_name'])? $additional['first_name']:'N/A'}}  </td>
                                    <td>Family Name : {{!empty($additional['last_name'])? $additional['last_name']:'N/A'}}  </td>
                                </tr>

                                <tr>
                                    <td>Other Names : N/A</td>
                                    <td>Gender : {{!empty($additional['gender'])? $additional['gender']:'N/A'}}  </td>

                                </tr>
                                <tr>
                                    <td>Year of Birth :{{!empty($additional['dob'])? $additional['dob']:'N/A'}}  </td>
                                    <td>Nationality : {{!empty($additional['country'])? getCityName($additional['country']):'N/A'}}</td>
                                </tr>

                            </tbody></table>
                    </div>
                </div>
            <?php }
        } else {
            ?>
            <h6><strong>No Additional Pax Detail</strong></h6>
<?php } ?>  
    </div> 

    <div class="col-sm-offset-2 col-sm-8">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="row">
                    <a href="{{ route('booking.booking-location-detail',['nItenaryId'=>$nBookId])}}" class="btn btn-primary btn-block">Next</a>
                </div>
            </div>
        </div>
    </div>
    @stop