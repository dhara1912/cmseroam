@foreach ($oItineraries as $aItinerary)
<?php $name = 'Guest'; ?>
<tr>
    <td>
        <label class="radio-checkbox label_check" for="checkbox-{{$aItinerary->order_id}}">
            <input type="checkbox" id="checkbox-{{$aItinerary->order_id}}" value="{{ $aItinerary->order_id }}" class="cmp_check">&nbsp;
        </label>
    <td> <a href="{{ route('booking.show-itenary',['nItenaryId'=>$aItinerary->order_id]) }}">{{$aItinerary->invoice_no}}</a></td>
    <td>{{ ($aItinerary->is_lead == 'Yes') ? $aItinerary->first_name . ' ' . $aItinerary->last_name : 'Guest' }}</td>
    <td>{{ date( 'd/m/y', strtotime( $aItinerary->created_at )) }}</td>
    <td>{{ date( 'd/m/y', strtotime( $aItinerary->from_date )) }}</td>
    <td>{{ date( 'd/m/y', strtotime( $aItinerary->to_date )) }}</td>

    <td>Itenary</td>
    <td>{{  getAgentName($aItinerary->agent_id)}}</td>
    <td>
     Archived 
    </td>
    <td class="text-center">
        <a href="{{ route('booking.booking-listing-detail',['nItenaryId'=>$aItinerary->order_id])}}" class="button success tiny btn-primary btn-sm">View</a>
    </td>
</tr>
@endforeach