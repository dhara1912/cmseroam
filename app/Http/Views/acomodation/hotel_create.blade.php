@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if(isset($oHotel))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Accommodation (Step 01 / 03)']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Accommodation']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
<?php //print_r($oHotel);exit; ?>
<div class="box-wrapper">
        <?php 
            $random_id =  (isset($nIdHotel) && $nIdHotel !='') ? $nIdHotel : 'rand_'.rand(10000, 99999);
        ?>
        <p>Accommodation General Details</p>
        @if(isset($oHotel))
            {{ Form::model($oHotel, array('url' => route('acomodation.hotel-create') ,'method'=>'POST','enctype'=>'multipart/form-data','id'=>'hotel_form')) }}
        @else
            {{Form::open(array('url' => route('acomodation.hotel-create'),'method'=>'Post','enctype'=>'multipart/form-data','id'=>'hotel_form')) }}
        @endif
        <input type="hidden" value="{{ $nIdHotel }}" name="hotel_id" />
        <input type="hidden" name="random_id" value="<?php echo $random_id; ?>"/>
        @if((auth::user()->type) !="admin" && auth::user()->type!="eroamProduct")
            <div class="form-group m-t-30">
				<label class="label-control">Domains <span class="required">*</span></label>
					<select  class="form-control" multiple="true" name="domains[]">
						@if(isset($DomainList) && $DomainList->count() > 0)
							@foreach($DomainList as $key=>$domain)
								<option @if(isset($oHotel))@if ( in_array( $domain->id, explode(",",$oHotel->domain_ids) ) ) selected   @endif  @endif	value="{{$domain->id}}"  id="domain_{{$domain->id}}" >{{$domain->name}}</option>
							@endforeach
						@endif	
					</select>
			</div>    
        @endif
        @if(auth::user()->type=="eroamProduct")   
        <div class="form-group m-t-30">
            <label class="label-control">Licensee <span class="required">*</span></label>
            {{Form::select('licensee',$licenseeList,Input::old('licensee'),['id'=>'licensee','class'=>'form-control'.' select-licensee','data-type'=>'licensee'])}}
        </div>
        
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">Name <span class="required">*</span></label>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>'form-control','placeholder'=>'Enter Name'])}}

        </div>
        <label for="name" generated="true" class="error" style="display:none">This field is required.</label>	
        <div class="form-group m-t-30">
            <label class="label-control">eRoam Code <span class="required">*</span></label>
            {{Form::text('eroam_code',Input::old('eroam_code'),['id'=>'eroam_code','class'=>'form-control','placeholder'=>'Enter eRoam Code'])}}
        </div>
        <label for="eroam_code" generated="true" class="error" style="display:none">This field is required.</label>

        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Supplier <span class="required">*</span></label>
                    {{Form::select('supplier_id',$oSupplier,Input::old('supplier_id'),['id'=>'supplier_id','class'=>'form-control'])}}
                </div>
                <label for="supplier_id" generated="true" class="error" style="display:none">This field is required.</label>
            </div>	

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Accommodation Group <span class="required">*</span></label>
                    {{Form::select('hotel_category_id',$oCategory,Input::old('hotel_category_id'),['id'=>'hotel_category_id','class'=>'form-control'])}}
                </div>
                <label for="hotel_category_id" generated="true" class="error" style="display:none">This field is required.</label>	
            </div>

        </div>

        <div class="form-group m-t-30">
            <label class="label-control">Address<span class="required">*</span></label>
            {{Form::text('address_1',Input::old('address_1'),['placeholder'=>'Enter Address 1', 'id'=>'address_1','class'=>'form-control'])}}	

        </div>
        <label for="address_1" generated="true" class="error" style="display:none">This field is required.</label>
        
        <div class="form-group m-t-30">
            <label class="label-control">Country <span class="required">*</span></label>
            {{Form::select('country_id',$oCountry,Input::old('country_id'),['id'=>'country_id','class'=>'form-control select-country'])}}
        </div>
        <label for="country_id" generated="true" class="error" style="display:none">This field is required.</label>
        
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">City <span class="required">*</span></label>
                    <select name="city_id" id="city_id" class='form-control select-city'>
                        <option value="">Select City</option>

                    </select>
                </div>
                <input type="hidden" id="city" value="{{ isset($oHotel) ? $oHotel->city_id : ''}}" />
                <label for="city_id" generated="true" class="error" style="display:none">This field is required.</label>		
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Postcode <span class="required">*</span></label>
                    {{Form::text('postcode',Input::old('postcode'),['placeholder'=>'Enter Postcode', 'id'=>'postcode','class'=>'form-control'])}}	
                </div>
                <label for="postcode" generated="true" class="error" style="display:none">This field is required.</label>	
            </div>	

        </div>

        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Email Address <span class="required">*</span></label>
                    {{Form::text('reception_email',Input::old('reception_email'),['placeholder'=>'Enter Email Address','id'=>'reception_email','class'=>'form-control'])}}	
                </div>
                <label for="reception_email" generated="true" class="error" style="display:none">This field is required.</label>				
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Contact Number <span class="required">*</span></label>
                    {{Form::text('reception_phone',Input::old('reception_phone'), ['placeholder'=>'Enter Contact Number', 'id'=>'reception_phone', 'class'=>'form-control'])}}
                </div>
                @if ( $errors->first( 'reception_phone' ) )
                    <small class="error">{{ $errors->first('reception_phone') }}</small>
                @endif
                <label for="reception_phone" generated="true" class="error" style="display:none">This field is required.</label>				
            </div>


        </div>
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">eRoam Stamp <span class="required">*</span></label>
                    <select name="eroam_stamp" id="eroam_stamp" class='form-control'>
                        <option value="0">No</option>
                        <option value="1">Yes</option>

                    </select>
                </div>	
                <label for="eroam_stamp" generated="true" class="error" style="display:none">This field is required.</label>				
            </div>	

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Approval Status <span class="required">*</span></label>
                    <select name="is_approved" id="is_approved" class='form-control'>
                        <option value="1">Yes</option>
                        <option value="0">No</option>

                    </select>
                </div>
                <label for="is_approved" generated="true" class="error" style="display:none">This field is required.</label>					
            </div>

        </div>
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">eRoam Rating <span class="required">*</span></label>
                    {{Form::number('ranking',Input::old('ranking'),['placeholder'=>'Enter eRoam Rating','id'=>'ranking','class'=>'form-control','step'=>'any','min'=>0])}}
                </div>	
                <label for="ranking" generated="true" class="error" style="display:none">This field is required.</label>				
            </div>	


            <div class="col-sm-6">
                <div class="form-group">
                    <label for="ratings" class="label-control">
                        Star Rating {{ Input::old('star_rating') }}
                    </label>
                    <div class="small-8 large-8 columns">
                        {{Form::text('star_rating',Input::old('star_rating'), [ 'id'=>'ratings', 'class'=>'rating','data-min'=>"0", 'data-max'=>"5", 'data-stars'=>"5", 'data-size'=>"xs", 'data-disabled'=>'false' ,'data-step'=>"1"])}}
                    </div>
                </div>	
            </div> 		
        </div> 
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Latitude</label>
                    {{Form::text('latitude',Input::old('latitude'),['placeholder'=>'Enter Latitude','id'=>'latitude','class'=>'form-control'])}}
                </div>	
            </div>	
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Longitude</label>
                    {{Form::text('longitude',Input::old('longitude'),['placeholder'=>'Enter Longitude','id'=>'longitude','class'=>'form-control'])}}
                </div>
            </div> 
        </div>
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Check-In Time</label>
                    {{Form::text('checkin_time',Input::old('checkin_time'),['placeholder'=>'Enter Checkin Time','id'=>'checkin_time','class'=>'form-control'])}}
                </div>  
            </div>  
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Check-Out Time</label>
                    {{Form::text('checkout_time',Input::old('checkout_time'),['placeholder'=>'Enter CheckOut Time','id'=>'checkout_time','class'=>'form-control'])}}
                </div>
            </div> 
        </div> 
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="currency_id" class="label-control"> Currency <span class="required">*</span></label>
                    {{Form::select('currency_id',$oCurrencies,Input::old('currency_id'),['id'=>'currency_id','class'=>'form-control'])}}
                </div>	
                <label for="currency_id" generated="true" class="error" style="display:none">This field is required.</label>				
            </div>	
        </div> 
        <div class="form-group m-t-30">

            <label class="label-control">Child Allowed <span class="required">*</span></label>
            <div>
                <label class="radio-checkbox label_radio" for="radio-03">
                    <input type="radio" id="radio-03" value="1" name="child_allowed" {{ (isset($oHotel) && $oHotel->child_allowed != 0) ? 'checked' : ''}}> Yes
                </label> 
                <label class="radio-checkbox label_radio" for="radio-04">
                    <input type="radio" id="radio-04" value="0" name="child_allowed" {{ (isset($oHotel) && $oHotel->child_allowed == 0) ? 'checked' : ''}}> No
                </label>
            </div>   
            <label for="child_allowed" generated="true" class="error" style="display:none">This field is required.</label> 
        </div>
        <div class="form-group m-t-30">
            <label class="label-control">Accommodation Introduction Description  (Max 250 Characters) <span class="required">*</span></label>
            {{ Form::textarea('intro_description',Input::old('intro_description'), ['placeholder' => 'Enter Introduction Description', 'rows' => '4','id'=>'intro_description','class'=>'form-control']) }}
        </div>
        <label for="intro_description" generated="true" class="error" style="display:none">This field is required.</label>	

        @if(Input::old('video_url'))
            @foreach(Input::old('video_url') as $key => $day)
            <div class="row tb_added">
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.video_url') }}</label>
                        <input placeholder="Enter Video URL" type="url" class="form-control" name="video_url[]"  value="{{$day}}">
                    </div> 
                </div>
            </div>
            @endforeach
        @elseif(isset($oVideos))
            @foreach($oVideos as $key => $day)
            <div class="row tb_added">
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.video_url') }}</label>
                        <input placeholder="Enter Video URL" type="url" class="form-control" name="video_url[]" value="{{ $day->original }}">
                    </div> 
                </div>
            </div>
            @endforeach
        @else
            <div class="row tb_added">
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.video_url') }}</label>
                        {{Form::url('video_url[]',Input::old('days'),['class'=>'form-control','placeholder'=>'Enter Video URL'])}}
                    </div> 
                </div>
            </div>
        @endif
        <div class="clones"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <i class="fa fa-minus-square right fa-2x" id="fa-minus" aria-hidden="true"></i>
                    <i class="fa fa-plus-square right fa-2x" id="fa-plus" aria-hidden="true"></i>
                </div> 
            </div>      
        </div>
	  		
        <div class="form-group m-t-30">
            <label class="label-control">Description</label>
            {{ Form::textarea('description',Input::old('description'), ['placeholder' => 'Enter Description', 'rows' => '4','id'=>'description','class'=>'form-control']) }}

        </div>
 	  	       
        <div class="form-group m-t-30">
            <label class="label-control">Special Instruction </label>
            {{ Form::textarea('notes',Input::old('notes'), ['placeholder' => 'Enter Notes', 'rows' => '4','id'=>'notes','class'=>'form-control']) }}		

        </div>

        <div class="form-group m-t-30">
            <label class="label-control">Check-In Instruction</label>
            {{ Form::textarea('checkin_instruction',Input::old('checkin_instruction'), ['placeholder' => 'Enter Notes', 'rows' => '4','id'=>'checkin_instruction','class'=>'form-control']) }}        

        </div>

        <div class="form-group m-t-30">
            <label class="label-control">Location Description</label>
            {{ Form::textarea('location_description',Input::old('location_description'), ['placeholder' => 'Enter Notes', 'rows' => '4','id'=>'location_description','class'=>'form-control']) }}        

        </div>

        <div class="form-group m-t-30">
            <label class="label-control">Amenities Description</label>
            {{ Form::textarea('amenities_description',Input::old('amenities_description'), ['placeholder' => 'Enter Notes', 'rows' => '4','id'=>'amenities_description','class'=>'form-control']) }}        

        </div>

        <div class="box-wrapper">
            <p>Location</p>
            <div class="form-group m-t-30">
               <label></label>
               <?php  $geo_location = (isset($oHotel) && isset($oHotel->geo_location)) ? $oHotel->geo_location : ((isset($oHotel)) ? $oHotel->name:''); ?>
               <input type="text" class="form-control" placeholder="Location Name" name="geo_location" id="input-location" value="{{ $geo_location }}">
            </div>
            <div class="form-group m-t-30">
                    <div id="hotel-map"></div>
                    <div id="infowindow-content">
                        <img src="" width="16" height="16" id="place-icon">
                        <span id="place-name" class="place-title"></span><br>
                        <span id="place-address"></span>
                    </div>
            </div>
          
        </div>
        
        <div class="m-t-20 row col-md-6 col-sm-8 pull-right">
            <div class="row text-right">

                <div class="col-sm-6">
                    <a href="{{ route('acomodation.hotel-list')}}" class="btn btn-primary btn-block">Cancel</a>
                </div>
                <div class="col-sm-6">
                    <button type="submit" class="button success btn btn-primary btn-block">Save & Next</button>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>	
        {{Form::close()}}
</div>
<div class="box-wrapper m-t-30">
    <p>Accommodation Images</p>
    <div class="m-t-20">
        <form enctype="multipart/form-data" id="image-form">
            <div class="file-upload1">
                <input type="hidden" id="hotelId" name="hotelId" value="{{ $random_id }}"/>
                <input type="file" class="file-input" id="hotelImage" name="hotelImage"/>
            </div>
            <span class="input-filename"></span>
            <span id="image-submit-load" style="display:none;">
                <i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw image-file-loader"></i>
            </span>  
        </form>
    </div>
    <div class="m-t-20">
        <input type="hidden" name="image_id"> 
        <div class="table-responsive" id="panel-drag">
            <table class="table table-center-all">
                <thead>
                    <tr>
                        <th>Re-Order</th>
                        <th>Preview</th>
                        {{-- <th>Download</th> --}}
                        <th>Remove</th>
                    </tr>
                </thead>
                @if($HotelImg->count()>0)
                    @foreach($HotelImg as $img)
                    <tbody class="sortable-list">  
                    @if($img->original!="") 
                    <tr data-id="{{$img->id}}" id="{{$img->id}}">
                                    <td><a href="#" class="sortable-icon"><i class="icon-reorder-black"></i></a></td>
                                    <td class="table-no-padding">
                                    <div class="table-image">
                                            <img src="{{ trans('messages.image_url',['image_title' => 'hotels/'.$img->original]) }}" class="img-responsive">
                                    </div>
                                    </td>            
                                    {{-- <td><a target="blank" href="{{ trans('messages.image_url',['image_title' => 'hotels/'.$img->original]) }}" class="action-icon" download><i class="icon-download"></i></a></td> --}}          
                                    <td><a href="javascript://" class="delete_tour_img action-icon image-click-event image-delete" data-type="hotelimg" data-action="delete" data-id="{{$img->id}}" id="{{$img->id}}"><i class="icon-delete-forever"></i></a></td>       
                                </tr>  
                       @endif         
                        </tbody>

                    @endforeach
                   @else
                   <tbody class="sortable-list">

                    </tbody>     
                @endif

            </table>
        </div>
    </div>
</div>	

@stop

@section('custom-css')
<link rel="stylesheet" href="{{ asset('assets/css/star-rating.min.css') }}">
<style>
    
    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }

    .success_message{
        color:green !important;
        text-align: center;
    }
    .inactive-stars{
        color: #cccccc;
        -webkit-text-stroke: 1px #777;
        text-shadow: 1px 1px #999;
    }
    #ratings{
        visibility: hidden;
    }
    .label.alert {
        background-color: #f04124;
        color: #FFFFFF;
    }
    .label.warning {
        background-color: #f08a24;
        color: #FFFFFF;
    }
    .label {
        display: inline-block;
        font-family: 'Open Sans', sans-serif;
        font-weight: normal;
        line-height: 1;
        margin-bottom: auto;
        position: relative;
        text-align: center;
        text-decoration: none;
        white-space: nowrap;
        font-size: 12px;
        background-color: #008CBA;
        color: #FFFFFF;
        padding: 5px 10px 5px 10px;
        margin-top: 10px;
    }
    .label.success {
        background-color: #43AC6A;
        color: #FFFFFF;
    }
    .label.secondary {
        background-color: #e7e7e7;
        color: #333333;
    }
    .rating-container .filled-stars {
        color: #27A9E0;
        /*border-color:#27A9E0;
        -webkit-text-stroke: 1px #27A9E0; */
    }
    #hotel-map {
        height: 450px;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }
</style>
<link href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" />
@stop

@section('custom-js')  
<script type="text/javascript" src="{{ asset('assets/js/moment.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}" ></script>
<script src="{{ asset('assets/js/star-rating.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>
<script>
tinymce.init({
    selector: '#notes',
    height: 250,
    menubar: false
});
tinymce.init({
    selector: '#intro_description',
    height: 250,
    menubar: false
});
tinymce.init({
    selector: '#checkin_instruction',
    height: 250,
    menubar: false
});
tinymce.init({
    selector: '#amenities_description',
    height: 250,
    menubar: false
});
tinymce.init({
    selector: '#location_description',
    height: 250,
    menubar: false
});
tinymce.init({
    selector: '#description',
    height: 250,
    menubar: false
});
</script>
  
<script>
$(document).ready(function(){
    $('#checkout_time').datetimepicker({ format: 'hh:ss:00' });
    $('#checkin_time').datetimepicker({ format: 'hh:ss:00' });
    $('.sortable-list').sortable({
                revert: true,
                connectWith: ".sortable-list",
                stop: function (event, ui) {

                    var image_id = new Array();
                    $('.sortable-list tr').each(function () {
                        id = $(this).attr('data-id');
                        image_id.push(id);
                    });

                    if (image_id) {
                        console.log(image_id);
                        $.post(siteUrl('sort-hotel-images/'), {
                            image_id: image_id, _token: '{{ csrf_token() }}',
                        });
                    }
                }
            });
    $("#hotel_form").validate({
        ignore: [],
        rules: {
            name: 'required',
            eroam_code: 'required',
            supplier_id: 'required',
            hotel_category_id: 'required',
            address_1: 'required',
            country_id: 'required',
            postcode: {
                number: true
            },
            reception_email: {
                required: true,
                email: true
            },
            reception_phone: 'required',
            eroam_stamp: 'required',
            is_approved: 'required',
            ranking: 'required',
            city_id: 'required',
            currency_id: 'required',
            child_allowed: 'required',
            domains: 'required'

        },

        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    
    $('input[name="hotelImage"]').fileuploader({

	        changeInput:'<div class="fileuploader-input">' +
	                        '<div class="fileuploader-input-inner">' +
	                            '<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Tour Images here to upload.</span></h3>' +
	                        '</div>' +
	                    '</div>' + 
	                    '<div class="fileupload-link"><span>Click here to upload images from your computer</span> (Dimension 1000 X 259, max file size, 2mb. Supported file types, .jpeg, .jpg, .png).</div>',
	        theme: 'dragdrop',
	        upload: {
	            url: siteUrl('acomodation/hotel-images'),
	            data: {hotel_id:$('#hotelId').val()},
	            type: 'POST',
	            enctype: 'multipart/form-data',
	            start: true,
	            synchron: true,
	            beforeSend: null,
	            onSuccess: function(result, item) {
	                var data = result;
                    var imageNamePath=data.data.imageNamePath;
                    var image_id=data.data.id;
                   
	                if(data.success === true){
	                    $('.fileuploader-items').hide();
	                    item.name = data.data[0].thumbnail;
	                    item.image_id = data.data[0].id;

	                    //Append to html
	                    var img_html = '<tr data-id="'+image_id+'" id="image_'+image_id+'">';
                                img_html +='<td><a href="#" class="sortable-icon"><i class="icon-reorder-black"></i></a></td>';
                                img_html +='<td class="table-no-padding">';
                                img_html +='<div class="table-image">';
                                img_html +='<img src="'+imageNamePath+'" class="img-responsive">';
                                img_html +='</div>';
                                img_html +='</td>';            
                                img_html +='<td><a target="blank" href="'+imageNamePath+'" class="action-icon" download><i class="icon-download"></i></a></td>';          
                                img_html +='<td><a href="javascript://" class="delete_tour_img action-icon image-click-event image-delete" data-action="delete" data-type="hotelimg" data-id="'+image_id+'" id="'+image_id+'"><i class="icon-delete-forever"></i></a></td>';          
	                        img_html +='</tr>';        
	                    $('.sortable-list').append(img_html);

//                            $('.image-click-event').click(function(){
//                                    removeImage($(this).attr('id'));
//                            });
	                }
                        else{
                            $('.fileuploader-items').hide();
                        }
	                // if warnings
	                if (data.hasWarnings) {
	                    for (var warning in data.warnings) {
	                        alert(data.warnings);
	                    }
	                    
	                    item.html.removeClass('upload-successful').addClass('upload-failed');
	                    // go out from success function by calling onError function
	                    // in this case we have a animation there
	                    // you can also response in PHP with 404
	                    return this.onError ? this.onError(item) : null;
	                }
	                
	                item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
	                setTimeout(function() {
	                    item.html.find('.progress-bar2').fadeOut(400);
	                }, 400);
	            },
	            onError: function(result, item,response) {
                        $('.error_message_image').html('<span>The tour image must be a file of type: jpeg, jpg, png.</span>')
	            },
	            onProgress: function(data, item) {
	                var progressBar = item.html.find('.progress-bar2');
	                
	                if(progressBar.length > 0) {
	                    progressBar.show();
	                    progressBar.find('span').html(data.percentage + "%");
	                    progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
	                }
	            },
	            onComplete: null,
	        },

	        captions: {
	            feedback: '<span><i class="icon-add-photos"></i> Drag Tour Images here to upload.</span>',
	            feedback2: '<span><i class="icon-add-photos"></i> Drag Tour Images here to upload.</span>',
	            drop: '<span><i class="icon-add-photos"></i> Drag Tour Images here to upload.</span>'
	        },
	    });
    var map, marker, infowindow, infowindowContent;
    //setBreadCrumbs();
    initMap();
});
function initMap() {
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('input-location'));
        map = new google.maps.Map(document.getElementById('hotel-map'), {  center: {lat: -34.397, lng: 150.644}, zoom: 13 });
        autocomplete.bindTo('bounds', map);

        // INFO WINDOW
        infowindow = new google.maps.InfoWindow();
        infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);

        // MARKER
        marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        // GEOCODE
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            address: $('#input-location').val(),
            region: 'no',
        }, function(results, status) {
            if (status.toLowerCase() == 'ok') {
                var service = new google.maps.places.PlacesService(map);
                service.getDetails({
                    placeId: results[0].place_id
                }, function(place, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        showPlaceOnMap(place);
                    }
                });
                
            }
        });

        // ON CLICK AUTOCOMPLETE
        autocomplete.addListener('place_changed', function(e) {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            showPlaceOnMap(place);
        });
    }

    function showPlaceOnMap(place) {
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);
    }

    $( '.select-country' ).change( function() {
        getCity();
    });
    
    function getCity(){
        var value = $( '.select-country' ).val();
        var city = $( '#city' ).val();
        //console.log(city.id);
        if(value != ''){
        $.ajax({
            url: "{{ route('common.get-cities-by-country') }}",
            method: 'post',
            data: {
                country_id: value,
                _token: '{{ csrf_token() }}'
            },
            success: function( response ) 
            {
                var selectCity = $('.select-city');
                selectCity.html('<option value="" selected disabled>Select City</option>');
                for(var i = 0; i < response.length; i++) {
                    var selected = '';
                    if(city == response[i].id) 
                        selected = "selected";

                    var html = '<option value="'+ response[i].id +'" '+selected+'>'+ response[i].name +'</option>';
                    $( '.select-city' ).append(html);
                }
            }
        });
    }
    }

    $(document).ready(function () {
        getCity();
        $('.clear-rating ').remove();
        $('.fileuploader-items').hide();

        $(document).on('click', '.delete_hotel_img', function () {
            var hotel_id = $(this).attr('data-id');
            if (confirm('Are you sure you want to remove this file?')) {
                $.post(siteUrl('hotel-images/' + hotel_id), {
                    _method: 'DELETE', _token: '{{ csrf_token() }}',
                });
                $(this).closest('tr').hide();
            }

        });

    });
    function checktRatingValue(thisObject) {

        var text = $(thisObject).find('option:selected').text();
        var matched;
        var rating = $('#ratings');
        if (matched = text.match(/\d[\s]*[\W][\s]*\d/)) {
            var first = parseInt(matched[0].charAt(0));
            var last = parseInt(matched[0].charAt(matched[0].length - 1));
            var numberOfDisabledStars = 5 - last;

            var disabled = '';
            rating.val(first);

            rating.rating('refresh', {disabled: false, min: 0, max: last, step: first, stars: last, size: 'xs'});
            for (counter = 1; counter <= numberOfDisabledStars; counter++) {
                disabled += '<span class="star"><i class="fa fa-star inactive-stars" > </i></span>';
            }
            $('.rating-stars').after('<div class="rating-stars">' + disabled + '</div> ');

        } else if (matched = text.match(/\d/)) {
            var number = parseInt(matched[0]);
            rating.val(number);
            rating.rating('refresh', {disabled: false, min: 0, max: number, step: number, stars: number, size: 'xs'});
        } else if (text.match(/category/i)) {
            rating.val(0);
            rating.rating('refresh', {disabled: true, min: 0, max: 5, step: 1, stars: 5, size: 'xs'});
            $('.filled-stars').css({width: '100%', color: '#cccccc'});
        } else {
            rating.val(0);
            rating.rating('refresh', {disabled: false, min: 0, max: 5, step: 1, stars: 5, size: 'xs'});
        }
        $('.clear-rating ').remove();
    }

    $('#fa-plus').click(function () {
        var count = $('.clones').children().length+1;
        $('.tb_added:last').clone().appendTo(".clones");
        var numItems = $('.tb_added').length;
        $('.tb_added:last input').val('');

    });
    $('#fa-minus').click(function () {
        var numItems = $('.tb_added').length;
        if (numItems > 1) {
            $('.tb_added:last').remove();
        }
    });
</script>
@stop
