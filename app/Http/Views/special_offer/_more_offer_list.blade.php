@php $offer_type = ['value_based_dollar' => 'Value Based ( <i class="fa fa-dollar"></i> )',
    'value_based_per' => 'Value Based ( <i class="fa fa-percent"></i> )',
    'term_based' => 'Term Based'
]; @endphp
@foreach ($specialOffer as $aOffer)	
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aOffer->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aOffer->id;?>" value="<?php echo $aOffer->id;?>">&nbsp;
            </label>
        </td>
        <td>{{ $aOffer->offer_name }} </td>
        <td>
            @if($aOffer->offer_type === 'term_based')
                {!! $offer_type[$aOffer->offer_type] !!} ( {{ $aOffer->term_option }} )
            @else
                {!! $offer_type[$aOffer->offer_type] !!}
            @endif
        </td>
        <td> {{ $aOffer->inventory_type }} </td>
        <td> {{ $aOffer->client }} </td>
        <td> {{ $aOffer->selling_start_date.' - '.$aOffer->selling_end_date }} </td>
        <td> {{ $aOffer->travel_start_date.' - '.$aOffer->travel_end_date }} </td>
        <td> {{ $aOffer->status }} </td>
        <td>
            <a href="{{ route('special-offer.create', ['id'=>$aOffer->id]) }}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.update_btn') }}</a>
        </td>
    </tr> 
@endforeach