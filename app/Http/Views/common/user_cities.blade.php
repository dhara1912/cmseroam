@extends( 'layout/mainlayout' )

@section('custom-css')

@stop

@section('content')
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_list_title', ['name' => 'City']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div> 
    </div>
    <br>
    @endif
	<form action="{{route('add-user-city-geodata')}}" method="post" id="add-city" name="add-city">
	@csrf
		<div class="box-wrapper" >
			<a href="{{ route('common.manage_user_geo_data').'?isreset=1' }}" class="btn btn-info"> &nbsp;Back&nbsp; </a>
			<div class="table-responsive m-t-20 table_record">
			<div class="box-wrapper" style="max-height:550px;overflow-y:auto;">
				<h5>City</h5>
					<div class="m-t-10 row countries">
						<div class="overlay"></div>
						@if(isset($user_country['countries']))
							@foreach($user_country['countries'] as $key=>$value)
								<div class="col-sm-12 m-t-10">
									<p><b>@if($value != 0){{callCountryName($value)->name}} @endif</b></p>
								</div>
								@foreach(callCityByCountryid($value) as $key1=>$value1)
									<div class="col-sm-6">
										<div class="form-group">
											<label class="radio-checkbox label_check m-t-10" for="cityid{{$value1->id}}">
												<input  type="checkbox" id="cityid{{$value1->id}}" <?php  if(isset($city['city']) && in_array($value1->id,$city['city'])){ echo 'checked';} ?> name="cityid[]" value="{{$value1->id.'|'.$value1->country_id.'|'.callCountryName($value)->region_id}}" />{{$value1->name}}
											</label>
										</div>
									</div>
								@endforeach
							@endforeach
						@else
							@if(isset($fullcountries) && count($fullcountries) >0)
									@foreach($fullcountries as $key=>$value)
											<div class="col-sm-12 m-t-10">
												<p><b>{{$value}}</b></p>
											</div>
											@foreach(callCityByCountryid($key) as $key1=>$value1)
												<div class="col-sm-6">
													<div class="form-group">
														<label class="radio-checkbox label_check m-t-10" for="cityid{{$value1->id}}">
															<input  type="checkbox" id="cityid{{$value1->id}}"  name="cityid[]" value="{{$value1->id.'|'.$value1->country_id.'|'.callCountryName($key)->region_id}}" />{{$value1->name}}
														</label>
													</div>
												</div>
											@endforeach
									@endforeach
							@endif
						@endif		
					</div>
			</div>
			</div>
			@if(Auth::user()->type=='eroamProduct')
				<input type="hidden" name="licensee_id" value="{{$license->licensee_id}}" />
			@endif
			<input type="hidden" name="domain_id" value="{{$domain_id}}" />
			<button class="btn btn-success" id="city-submit">Save</button>
		</div>
	</form>
</div>
@stop