@extends( 'layout/mainlayout' )

@section('custom-css')

@stop

@section('content')
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_list_title', ['name' => 'Region']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div> 
    </div>
    <br>
    @endif
	<form action="{{route('add-user-region-geodata')}}" method="post" id="add-region" name="add-region">
	@csrf
		<div class="box-wrapper">
				<a href="{{ route('common.manage_user_geo_data').'?isreset=1' }}" class="btn btn-info"> &nbsp;Back&nbsp; </a>
			<div class="table-responsive m-t-20 table_record">
				<div class="box-wrapper">
				<h5>Regions</h5>
				<div class="m-t-10 row">
					@foreach($regions as $rKey => $region)
					<div class="col-sm-6">
						<div class="form-group">
							<label class="radio-checkbox label_check m-t-10 @if(isset($sel_regions) && in_array($rKey,$sel_regions)) c_on @endif" for="region{{$rKey}}">
							<input class="georegions" <?php  if(isset($user_region['regions']) && in_array($rKey,$user_region['regions'])){ echo 'checked';} ?> type="checkbox" name="region[]" id="region{{$rKey}}" value="{{$rKey}}"	>{{$region}}
							</label>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			</div>
			<input type="hidden" name="domain_id" value="{{$domain_id}}" />
			@if(Auth::user()->type=='eroamProduct')
				<input type="hidden" name="licensee_id" value="{{$license->licensee_id}}" />
			@endif
			<button class="btn btn-success" id="region-submit">Save</button>
		</div>
	</form>
</div>
@stop