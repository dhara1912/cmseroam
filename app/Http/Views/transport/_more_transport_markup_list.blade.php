<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCountrySort(this,'hm.name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'hm.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'tm.allocation_type');">ALLOCATION TYPE
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'tm.allocation_type')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'tmp.percentage');">MARKUP PERCENTAGE 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'tmp.percentage')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'tmac.percentage');">AGENT COMMISSION PERCENTAGE 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'tmac.percentage')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getCountrySort(this,'is_default');">DEFAULT 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'is_default')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getCountrySort(this,'is_active');">ACTIVE
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'is_active')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">{{ trans('messages.thead_action')}}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
    @if(count($oTransportList) > 0)
        @foreach ($oTransportList as $aTransport) <?php //echo '<pre>'; print_r($oTransportList);exit; ?>		
            <tr>
                <td>
                    <label class="radio-checkbox label_check" for="checkbox-{{ $aTransport->id }}">
                        <input type="checkbox" class="cmp_check" id="checkbox-{{ $aTransport->id }}" value="{{ $aTransport->id }}">&nbsp;
                    </label>
                </td>
                <td>
                    <a href="{{ route('transport.transport-markup-create',[$aTransport->id]) }}">{{ $aTransport->name }}</a>
                </td>
                <td>{{ $aTransport->allocation_name }}</td>
                <td>{{ $aTransport->markup_percentage.'%' }}</td>
                <td>{{ $aTransport->agent_commition.'%' }}</td>
                <td>
                    <?php if ($aTransport->is_default): ?>
                        <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                    <?php else: ?>
                        <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                    <?php endif; ?>
                </td>								
                <td>
                    <?php if ($aTransport->is_active): ?>
                        <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                    <?php else: ?>
                        <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                    <?php endif; ?>
                </td>
                <td>
                    <a href="{{ route('transport.transport-markup-create',['nId'=>$aTransport->id]) }}" class="button success tiny btn-primary btn-sm pull-left m-r-10" >{{ trans('messages.update_btn') }}</a>
                </td>	
            </tr>
        @endforeach
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oTransportList->count() , 'total'=>$oTransportList->count() ]) }}</p></div>

</div>