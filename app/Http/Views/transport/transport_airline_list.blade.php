@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.airlines', ['name' => 'Transport Markup']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div> 
    </div>
    <br>
    @endif
    <div class="box-wrapper">
        <a href="{{ route('transport.airline-create') }}" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>
        <!--<p>{{ $oTransportAirlineList->count().' '. trans('messages.activity')  }}</p>-->
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                    <input type="text" class="form-control m-t-10" placeholder="Search Airlines" name="search_str" value="{{ $sSearchStr }}">
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                <select name="search-by" class="form-control m-t-10 search_by">
                    <option value="airline_code" {{ $sSearchBy == 'airline_code' ? 'selected' : '' }}>Code</option>
                    <option value="airline_name" {{ $sSearchBy == 'airline_name' ? 'selected' : '' }}>Name</option>
                    <option value="country_name" {{ $sSearchBy == 'country_name' ? 'selected' : '' }}>Country</option>
                </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <button class="btn btn-default btn-sm" type="submit" onclick="getMoreListing(siteUrl('transport/airlines'),event,'table_record');"><i class="icon-search-domain"></i></button>
            </div>
        </div>
        <div class="m-t-30">
            <label>{{ trans('messages.show_record') }}</label>
            <select class="select-entry" name="show_record" onchange="getMoreListing(siteUrl('transport/airlines?page=1'),event,'table_record');">
                <option value="10" {{ ($nShowRecord == 10) ? 'selected="selected"' : '' }}>10</option>
                <option value="20" {{ ($nShowRecord == 20) ? 'selected="selected"' : '' }}>20</option>
                <option value="30" {{ ($nShowRecord == 30) ? 'selected="selected"' : '' }}>30</option>
                <option value="40" {{ ($nShowRecord == 40) ? 'selected="selected"' : '' }}>40</option>
            </select>
            <label>{{ trans('messages.entries') }}</label>
        </div>
        <div class="table-responsive m-t-20 table_record">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            <label class="radio-checkbox label_check" for="checkbox-00">
                                <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                            </label>
                        </th>
                        <th onclick="getAirlineSort(event,this,'airline_code');">{{ trans('messages.code') }} 
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'airline_code')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
                        </th>
                        <th onclick="getAirlineSort(event,this,'airline_name');">{{ trans('messages.name') }} 
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'airline_name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
                        </th>
                        <th onclick="getAirlineSort(event,this,'country_name');">{{ trans('messages.country') }}
                            <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'country_name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
                        </th>
                        <th class="text-center">{{ trans('messages.thead_action')}}</th>
                    </tr>
                </thead>
                <tbody class="city_list_ajax">
                    @include('WebView::transport._more_transport_airline_list')
                </tbody>
            </table>
      
        </div>
        <div class="clearfix">
            <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oTransportAirlineList->count() , 'total'=>$oTransportAirlineList->total() ]) }}</p></div>
            <div class="col-sm-7 text-right">
                <ul class="pagination">
                    
                </ul>
            </div>
        </div>
    </div>
</div>
@stop
@section('custom-js')
<script type="text/javascript">

function getAirlineSort(event,element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc')
        
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreListing('{{ route('transport.airlines')}}',event,'table_record');
}
$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});  
$(function() {
    $('.pagination').pagination({
        pages: {{ $oTransportAirlineList->lastPage() }},
        itemsOnPage: 10,
        currentPage: {{ $oTransportAirlineList->currentPage() }},
        displayedPages:2,
        edges:1,
        onPageClick(pageNumber, event){
            if(pageNumber > 1)
            getMoreListing('{{ route('transport.airlines')}}?page='+pageNumber,event,'city_list_ajax');
            else
            getMoreListing('{{ route('transport.airlines')}}?page='+pageNumber,event,'city_list_ajax');
            $('#checkbox-00').prop('checked',false);
        }
    });
});
</script>
@stop