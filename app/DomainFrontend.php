<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomainFrontend extends Model
{
    protected $table = 'domain_frontend';

    public function webfont(){
    	return $this->belongsTo('App\WebFont');
    }

    public function domain() {
    	return $this->belongsTo('App\Domain','domain_id');
    }
}
