<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderSpecialNote extends Model
{
    protected $fillable = [
       'provider_id','special_desc'
    ];
    protected $table = 'tblproviderspecialnotes';
    protected $primaryKey = 'special_note_id';
    public $timestamps = false;
}
