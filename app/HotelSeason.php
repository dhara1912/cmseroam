<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class HotelSeason extends Model
{
    protected $table = 'zhotelseasons';
    protected $guarded = [];
    protected $primaryKey = 'id';
    
    public function supplier(){
    	return $this->belongsTo('App\HotelSupplier','hotel_supplier_id','id');
    }
    public function currency(){
        return $this->belongsTo('App\Currency');
    }
    
    public static function geHotelSeasonList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {   $user_type=Auth::user()->type;
        return HotelSeason::from('zhotelseasons as hs')
                    ->leftJoin('zcurrencies as cu','cu.id','=','hs.currency_id')
                    ->leftJoin('zhotelsuppliers as s','s.id','=','hs.hotel_supplier_id')
                    ->leftJoin('zhotel as h','h.id','=','hs.hotel_id')
                    ->leftJoin('zcities as c','c.id','=','h.city_id')
                    ->leftJoin('zcountries as co','co.id','=','c.country_id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })                       
                    ->where(function($query) use ($user_type)  
                    {
                        if(isset($user_type) && $user_type!='admin' && $user_type!="eroamProduct") {
                            $query->where('hs.user_id', Auth::user()->id);
                        }
                    })        
                    ->select(
                        'hs.id as id',
                        'hs.name as name',
                        'h.eroam_code as eroam_code',
                        's.name as supplier_name',
                        'hs.from as from',
                        'hs.to as to',
                        'hs.domain_ids as domains'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }
}
