<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ActivityMarkupAgentCommission extends Model
{
    protected $table = 'zactivitymarkupagentcommissions';
    protected $primaryKey = 'id';
    protected $fillable = ['activity_markup_id','percentage'];
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
