<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YalagoCountries extends Model
{
    protected $fillable = [
        'countryId','yalagoCountryId','countryCode','countryName'
    ];
    protected $table = 'yalago_countries';
    protected $primaryKey = 'countryId';
}
