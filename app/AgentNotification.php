<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class AgentNotification extends Model
{
    protected $table = 'agent_notifications';
    protected $primaryKey = 'booking_id';
    protected $guarded = [];
	
	
}