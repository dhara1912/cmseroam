<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HBZone extends Model
{
    protected $fillable = [
        'hb_destination_id','zone_name','zone_number'
    ];
    protected $table = 'zhbzones';
    protected $primaryKey = 'id';
}
