<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YalagoLocations extends Model
{
    protected $fillable = [
        'locationId','provinceId','countryId','locationName','yalagoLocationId','yalagoCountryId','yalagoProvinceId',
    ];
    protected $table = 'yalago_locations';
    protected $primaryKey = 'locationId';

    public static function getYalagoCity($name) {
        return YalagoLocations::from('yalago_locations as yl')
        			->join('yalago_provinces as yp','yl.yalagoProvinceId','=','yp.yalagoProvinceId')
                    ->join('yalago_countries as yc','yl.yalagoCountryId','=','yc.yalagoCountryId')
                    ->where('yl.locationName',$name)
                    ->select('yl.yalagoLocationId','yl.locationName', 'yl.yalagoProvinceId', 'yp.provinceName', 'yl.yalagoCountryId','yc.countryName','yc.countryCode')
                    ->orderBy('yl.locationName')
                    ->get();
        
    }

    public static function getYalagoCityName($LocationId) {
        return YalagoLocations::from('yalago_locations as yl')
                    ->join('yalago_provinces as yp','yl.yalagoProvinceId','=','yp.yalagoProvinceId')
                    ->join('yalago_countries as yc','yl.yalagoCountryId','=','yc.yalagoCountryId')
                    ->where('yl.yalagoLocationId',$LocationId)
                    ->select('yl.locationName', 'yl.yalagoProvinceId', 'yp.provinceName', 'yl.yalagoCountryId','yc.countryName','yc.countryCode')
                    ->orderBy('yl.locationName')
                    ->get();
        
    }
    
    public function yalagoCountry(){
        return $this->hasOne('App\YalagoCountries', 'yalagoCountryId', 'yalagoCountryId');
    }
    
    public function yalagoProvince()
    {
        return $this->hasMany('App\YalagoProvinces', 'yalagoProvinceId', 'yalagoProvinceId');
    }
}
