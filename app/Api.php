<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Api extends Model
{
    protected $table = 'zapis';

	protected $fillable = ['name', 'description', 'logo', 'type'];

	public function settings() {
		return $this->hasOne('App\ApiSettings');
	}
}

