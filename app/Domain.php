<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $table = 'domains';

    public function products(){
    	return $this->hasMany('App\DomainProduct','domain_id')->with('product');
    }

    public function account() {
    	return $this->hasOne('App\LicenseeAccount','licensee_id');
    }
    
    public function frontend() {
    	return $this->hasOne('App\DomainFrontend','domain_id');
    }

    public function location() {
    	return $this->hasMany('App\DomainLocation','domain_id')->with(['location_country','location_city']);
    }

    public function backend() {
    	return $this->hasMany('App\Backend','domain_id')->with('config');
    }

    public function inventory() {
    	return $this->hasMany('App\DomainInventory','domain_id')->with('inventory_config');
    }

    public function licenseeAccount() {
        return $this->hasOne('App\LicenseeAccount','licensee_id','licensee_id');
    }

    public function licenseeCommission() {
        return $this->hasMany('App\LicenseeCommission','licensee_id','licensee_id')->with('products');
    }
}
